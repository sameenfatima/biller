module ApplicationHelper
	def get_quantity(order_id, item_id)
		order_item = OrderItem.where(order_id: order_id, item_id: item_id).first
		order_item.quantity
	end
end
