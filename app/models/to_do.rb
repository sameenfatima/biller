class ToDo < ActiveRecord::Base
  belongs_to :store
  belongs_to :user

  acts_as_sequenced scope: :store_id

  # Automatically use the sequential ID in URLs
  def to_param
    self.sequential_id
  end
end
