class Store < ActiveRecord::Base
  belongs_to :admin, class_name: 'User'
  has_many :users
  has_many :stock_histories
  has_many :items
  has_many :orders
  has_many :employees, -> { employees }, class_name: 'User' # provide condition, so that admin is not listed, or make a class method
  has_many :to_dos
  has_many :expenses
  has_one :logo, class_name: 'Document', foreign_key: :owner_id

  validates :name, presence: true
end
