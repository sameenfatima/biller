class Item < ActiveRecord::Base
  include ActiveModel::Transitions
  include PublicActivity::Model
  tracked except: :update, owner: Proc.new{ |controller, model| controller.current_user }
  tracked store_id: Proc.new { |controller, model| controller.current_user.store_id }

  acts_as_sequenced scope: :store_id

  belongs_to :store
  belongs_to :category
  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :delete_all
  has_many :order_items
  has_many :orders, through: :order_items
  has_many :stock_histories, dependent: :delete_all
  has_many :documents, as: :owner

  validates :name, :sell_price, presence: true
  validates_numericality_of :stock, greater_than_or_equal_to: 0, message: 'Invalid stock'
  validates_uniqueness_of :name, scope: :store_id, message: 'An item with same name exists'

  scope :most_used_items, -> { order('sale_count') }

  state_machine do
    state :available   # first one is initial state
    state :low_stock
    state :out_of_stock#, exit: :exit_out_of_stock
    state :discontinued#, enter: lambda { |item| item.cancel_orders }

    event :available do
      transitions to: :available, from: [:low_stock, :out_of_stock, :discontinued], guard: lambda { |item| item.stock > 0 }
    end
    event :low_stock, success: :notify_low_stock do
      transitions to: :low_stock, from: [:available], guard: lambda { |item| item.stock <= item.stock_threshold }
    end
    event :out_of_stock, success: :notify_out_of_stock do
      transitions to: :out_of_stock, from: [:available, :low_stock], guard: lambda { |item| item.stock == 0 }
    end
    event :discontinued do
      transitions to: :discontinued, from: [:available, :low_stock, :out_of_stock]
    end
  end

  def add_stock quantity, unit_price, total_price
    transaction do
      stock_histories.create(store_id: store_id, stock_quantity: quantity, unit_price: unit_price, total_price: total_price)
      create_activity(key: 'item.add_stock', params: { stock_quantity: quantity })
      update(stock: stock + quantity)
      update_state
    end
  end

  def sale quantity
    stock_histories.create(store_id: store_id, stock_quantity: -1 * quantity, unit_price: sell_price, total_price: sell_price * quantity)
    update_state
  end

  private
  def update_state
    if stock == 0
      self.out_of_stock! unless self.out_of_stock?
    elsif stock > 0 && stock <= stock_threshold
      self.low_stock!    unless self.low_stock?
    elsif stock > stock_threshold
      self.available!    unless self.available?
    end
  end

  def notify_low_stock
    self.create_activity(key: 'item.low_threshold')
    ToDo.create(title: "Low stock", description: "Restock item", store_id: store_id, trackable_type: "Item", trackable_id: id)
  end

  def notify_out_of_stock
    self.create_activity(key: 'item.out_of_stock')
    ToDo.create(title: "Out of stock", description: "Restock item", store_id: store_id, trackable_type: "Item", trackable_id: id)
  end
end
