class Order < ActiveRecord::Base
  include ActiveModel::Transitions
  include PublicActivity::Model
  tracked except: :update, owner: Proc.new{ |controller, model| controller.current_user }
  tracked store_id: Proc.new {|controller, model| controller.current_user.store_id }

  acts_as_sequenced scope: :store_id

  belongs_to :store
  belongs_to :user
  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :delete_all
  has_many :order_items, dependent: :destroy
  has_many :items, through: :order_items

  default_scope { order('created_at DESC') }

  state_machine do
    state :new   # first one is initial state
    state :draft
    state :paid
    state :return
    state :void

    event :draft do
      transitions to: :draft, from: :new, on_transition: :process
    end
    event :paid do
      transitions to: :paid, from: :draft, on_transition: :paid
    end
    event :return do
      transitions to: :return, from: :new, on_transition: :return
    end
    event :void do
      transitions to: :void, from: [:draft, :paid, :return]#, on_transition: :mark_void # needs to discuss this and find solution for draft, paid, return
    end
  end


  private
  def calculate_total
    self.update(total: order_items.joins(:item).sum('items.sell_price * order_items.quantity'))
  end

  def paid payment
    self.update(cashout_time: DateTime.now, payment: payment)
  end

  def process items_hash
    Order.transaction do
      self.save
      items_hash.each do |item_id, quantity|
        quantity = quantity.to_f
        if quantity > 0
          item = Item.find_by(id: item_id) # user store.items
          order_items.create(item_id: item_id, quantity: quantity)
          unless item.update(stock: (item.stock - quantity), sale_count: (item.sale_count + quantity))
            raise ActiveRecord::Rollback
          end
          item.sale quantity
        end
      end
      calculate_total
    end
  end

  def return items_hash
    Order.transaction do
      self.save
      items_hash.each do |item_id, quantity|
        if quantity.to_f < 0
          item = Item.find_by(id: item_id) # user store.items
          order_items.create(item_id: item_id, quantity: quantity)
          unless item.update(stock: (item.stock - quantity.to_f))
            raise ActiveRecord::Rollback
          end
        end
      end
      calculate_total
    end
  end


end
