class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  acts_as_sequenced scope: :store_id

  # validates :name, presence: true
  validates :email, presence: true
  validates :email, :format => { :with => /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/ }

  belongs_to :store
  has_many :orders
  has_many :to_dos
  has_many :expenses
  has_many :attendences

  scope :employees, -> { joins(:store).where('users.id != stores.admin_id') }
  # scope :active, -> { where(is_active: true) }
  
  def is_admin?
    store.admin_id == id
  end

  def self.mark_present user_ids
    user_ids.each do |user_id|
      self.find_by(id: user_id).attendences.create
    end
    # inserts = []
    # self.select(:id).each do |emp|
    #   inserts.push "(#{emp.id})"
    # end
    # sql = "INSERT INTO attendences (user_id) VALUES #{inserts.join(", ")}"
    # ActiveRecord::Base.connection.execute(sql)
  end

  def self.mark_absent user_ids
    user_ids.each do |user_id|
      self.find_by(id: user_id).attendences.where("created_at >= ?", Time.zone.now.beginning_of_day).destroy_all
    end
  end

end
