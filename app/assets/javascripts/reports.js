var margin = { top: 20, right: 20, bottom: 30, left: 40 },
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

// d3js uses chaining, that means whn we call a function the object on which the function was called
// is returned, so we can call multiple methods in chains.

// x and y will act as the scales that are shown on the sides of the graph
var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], 0.1); // ordianal scales means the it will have names,words etc... in our case it will have time.

var y = d3.scale.linear()
    .range([height,0]);  // linear scale is simple numerical scale

$(document).ready(function(){
$.ajax({
           type: "GET",
           contentType: "application/json; charset=utf-8",
           url: 'reports/order_by_hour',
           dataType: 'json',
           success: function (data) {
               draw(data);
           },
           error: function (result) {
               error();
           }
       });


var xAxis = d3.svg.axis() 
    .scale(x)
    .orient("bottom"); //here we have assigned axis to the svg element

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(10); //here we have assigned axis to the svg element, ticks mean the number of points, 1,2,..10

var svg = d3.select("svg")
    .attr("width", width + margin.left )
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")"); // setting attributes for the svg element 



function draw(data) {
  console.log(data);
  x.domain(data.map(function(d){return d.time;})); // seeting the domain of the axis, domain will be mapped on the range, we set the range above.
  y.domain([0, d3.max(data, function(d) { return d.sales; })]); // setting domain for the ordinal scale, first word will be mapped to first tick

   svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis); //adding the axis to the svg element. "g" means group.

   svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", -35)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Sales");

   svg.selectAll(".bar") 
      .data(data) //adding bars for the data, for every data element data(data) function will introduce a 
                  // a psudo element at its palce 
    .enter().append("rect") // here we have given the real shape to psudo elements
      .attr("class", "bar") 
      .attr("x", function(d) { return x(d.time); }) //setting "x" value for the bar, means where it will appear on x axis
      .attr("width", x.rangeBand()) 
      .attr("y", function(d) { return y(d.sales); }) //y value
      .attr("height", function(d) { return height - y(d.sales); });

  }

  function error() {
    console.log("error")
  }

});
