// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks
//= require d3
//= require_tree .

$(document).ready(function() {

  $(".heading").on('click', function(e){
    e.preventDefault();
    console.log(e.target);
    var $target = $(e.target);
    if(!$target.hasClass('heading')){
      $target = $target.parents(".heading");
      if($target.hasClass("heading_open")){
        $($target.parents('.card')).animate({
          height: $($target.parents('.card')).data('h') + "px"
        },function(){
          $target.removeClass('heading_open');
          $a = $target.find('.close');
          $a.html('keyboard_arrow_up')
          // $a.removeClass('mdi-hardware-keyboard-arrow-down');
          // $a.addClass('mdi-hardware-keyboard-arrow-up');

          $($target.parents('.card')).css("height", "auto"); // show more bug fix
        });
      }
      else{
        $($target.parents('.card')).data('h',$target.parents('.card').height());
        $($target.parents('.card')).animate({
          height: "40px"
        },function(){
          $target.addClass('heading_open');
          $a = $target.find('.close');
          // $a.removeClass('mdi-hardware-keyboard-arrow-up');
          // $a.addClass('mdi-hardware-keyboard-arrow-down');
          $a.html('keyboard_arrow_down');
        });
      }
    }
  });

  $('.expand_more').on('click', function(e){
    $('.hidden-search-menu').toggle('fast');
    $('.expand_more').toggle();
    $('.expand_less').toggle();
  });
  $('.expand_less').on('click', function(e){
    $('.hidden-search-menu').toggle('fast');
    $('.expand_more').toggle();
    $('.expand_less').toggle();
  });


  $('.modal-trigger').leanModal();
  $('select').material_select();

  // Initialize collapse button
  $(".button-collapse").sideNav();
    Materialize.toast('Welcome to PiStore!', 4000) // 4000 is the duration of the toast

  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 25,    // Creates a dropdown of 15 years to control year
    container: 'body',   // appends DatePicker to Body Element, bug: https://github.com/Dogfalo/materialize/issues/1032
    onClose: function(){
      // picker auto reopens, bug: https://github.com/amsul/pickadate.js/issues/160 
      $(document.activeElement).blur();
    },
    onSet: function(context) {
      // autoclose on date select, https://github.com/Dogfalo/materialize/issues/1331
      $('.datepicker').close();
    }
  });

  /******** a standard implementation of checkall *******/
  $('.mark_all').on('click', function(){
    $(':checkbox[name="' + $(this).data('checkbox-name') + '"]').prop("checked", true);
  });
  $('.mark_none').on('click', function(){
    $(':checkbox[name="' + $(this).data('checkbox-name') + '"]').prop("checked", false);
  });

  $(':checkbox.selectall').on('click', function(){
    $(':checkbox[name="' + $(this).data('checkbox-name') + '"]').prop("checked", $(this).prop("checked"));
  });
  $(':checkbox.checkme').on('click', function(){
    var _selectall = $(this).prop("checked");
    if ( _selectall ) {
      $( ':checkbox[name="' + $(this).attr('name') + '"]' ).each(function(i){
        _selectall = $(this).prop("checked");
        return _selectall;
      });
    }
    $(':checkbox[name="' + $(this).data('select-all') + '"]').prop("checked", _selectall);
  });
  /******** a standard implementation of checkall *******/

});


// update shortcut library (maybe), and update shorcuts
key('ctrl+o', function(){
	window.location = '/orders/new'
	return false;
});

key('ctrl+s', function(){
	$('#new_order').submit();
	return false;
});

key('ctrl+space', function(){
	window.location = '/items'
	return false;
});

