function addOrInsertToOrder(item){
  if(item.stock <= 0 ){
    alert('Item out of Stock');
    return;
  }
  if($("[name='item_ids["+ item.id +"]']").length > 0){
    if($("[name='item_ids["+ item.id +"]']")[0].value <= $("[name='item_ids["+ item.id +"]']")[0].max ){
      $("[name='item_ids["+ item.id +"]']")[0].value = $("[name='item_ids["+ item.id +"]']")[0].valueAsNumber + 1;
    }
    else{
      alert('Item stock empty');
    }
  }
  else{
    $(".order-items-list tbody").append(
      "<tr id= " + item.id + "> \
        <td>" + item.name + "</td> \
        <td>" + item.sell_price + "</td> \
        <td class='col-xs-3'> \
          <input class='item-count' step='any' min=0 max=" + item.stock + " type='number' name='item_ids[" + item.id + "]' value=1 /> \
        </td> \
        <td> \
          <i class='material-icons' onclick='$(this).parent().parent().remove()'>clear</i> \
        </td> \
      </tr>");
  }
}

function addItemToOrder(item){
  var item_obj = new Object();

  item_obj.id         = item.data('item-id');
  item_obj.name       = item.data('item-name');
  item_obj.sell_price = item.data('item-price');
  item_obj.stock      = item.data('item-stock');

  addOrInsertToOrder(item_obj);
}

$(document).ready(function(){
  // switched to keyup, keypress have a bug, it misses the char which is pressed now
  $("#search").on("keyup", function(event){
      $.ajax({
        type: "GET",
        url: "/items",
        data: { 'query': $(this).val() },
        dataType: "json",
        success: function( response ) {
          searched_result_html = '';
          for(i=0 ; i < response.length ; i++){
            searched_result_html += "<tr id= "+ response[i].id +"><td class=item-name>" + response[i].name + "</td><td class=item-price>" + 
                response[i].sell_price + "</td><td>\
                 <a class='add-item-in-order add-item btn-floating btn-small waves-effect waves-light' \
                 onclick='addItemToOrder($(this))' \
                 data-item-id=" + response[i].id + " \
                 data-item-name='" + response[i].name + "' \
                 data-item-price=" + response[i].sell_price + " \
                 data-item-stock=" + response[i].stock + " >\
                  <i class='material-icons'>add</i> \
                </a></td></tr>";
          }
          $(".seached-items-table tbody").html(searched_result_html);
        },
        error: function(xhr, status, error) {
          console.log("error while searching item...");
        }
      });
  });
});
