$(document).ready(function(){

	$(document).on("change", '#item_category_id', function(event){
		if( $(this).val() == '2' ){
			$("#item-code-div").hide();
			$('#item_code').removeAttr('required')
			// $("#item-buy-price-div").hide();
		}
		else{
			$("#item-code-div").show();
			$('#item_code').attr('required', 'required')
			// $("#item-buy-price-div").show();
		}
	});	

	$('.batch_action_items').on('click', function(e){
		e.preventDefault();
		$('#batch_action').val( $(this).data('batch-action') );
		$('#items_batch_action_form').submit();
	})

});
