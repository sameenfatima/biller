$(document).ready(function() {
  $('body .edit-employee-class').on('click', function(e){
    var emp_id = $(this).data('id');

    $('#edit-employee-sequential-id').val(emp_id);
    $('#edit-employee-name').val( $('#employee-name-' + emp_id).html() );
    $('#edit-employee-email').val( $('#employee-email-' + emp_id).html() );
    $('#edit-employee-monthly-salary').val( $('#employee-monthly-salary-' + emp_id).html() );
    $('#edit-employee-joined').val( $('#employee-joined-' + emp_id).html() );
  });

  $('#mass_mark_present').on('click', function(e){
    var selected_seq_ids = $('input[name="employee-checkbox"]:checked').map(function() { return $(this).data('sequential-id'); }).get();

    $.getJSON('/store/mark_present', { sequential_ids: selected_seq_ids })
      .done(function(json){
        alert('success');
      })
      .fail(function( jqxhr, textStatus, error ) {
        var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
        alert('Error');
      });
  });

  $('#mass_mark_absent').on('click', function(e){
    var selected_seq_ids = $('input[name="employee-checkbox"]:checked').map(function() { return $(this).data('sequential-id'); }).get();

    $.getJSON('/store/mark_absent', { sequential_ids: selected_seq_ids })
      .done(function(json){
        alert('success');
      })
      .fail(function( jqxhr, textStatus, error ) {
        var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
        alert('Error');
      });
  });
});