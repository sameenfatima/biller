// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function() {
    var pressed = false; 
    var chars = []; 
    $(window).keypress(function(e) {
      if (e.which >= 48 && e.which <= 57) {
        chars.push(String.fromCharCode(e.which));
      }
      if (pressed == false) {
        setTimeout(function(){
          if (chars.length >= 10) {
            var barcode = chars.join("");
            $.getJSON( "/items/find_by_code", { code: barcode , dataType: 'json' } )
              .done(function( json ) {
                console.log( "JSON Data: " + json.items.length );
                if(json.items.length > 0){
                  for(i=0 ; i < json.items.length ; i++ ){

                    var items = $("[name='item_ids["+ json.items[i].id +"]']");
                    if(items.length > 0){
                      $("[name='item_ids["+ json.items[i].id +"]']")[0].value = $("[name='item_ids["+ json.items[i].id +"]']")[0].valueAsNumber + 1;
                    }
                    else{
                      html = "";
                      if(json.items[i].item_type == 'bakery'){
                        html = "<tr id= " + json.items[i].id + "><td>" + json.items[i].title + "</td><td>" + json.items[i].sell_price + "</td><td class='col-xs-3'><input class='form-control' step='any' min=0 type='number' name='item_ids["+ json.items[i].id +"]' value=1 /></td></tr>";
                      }
                      else{
                        html = "<tr id= " + json.items[i].id + "><td>" + json.items[i].title + "</td><td>" + json.items[i].sell_price + "</td><td class='col-xs-3'><input class='form-control' min=0 type='number' name='item_ids["+ json.items[i].id +"]' value=1 /></td></tr>";
                      }
                      $(".order-items-list tbody").append(html)
                    }
                  }
                }
                else{ alert('Item Not Found') }
              })
              .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                console.log( "Request Failed: " + err );
                alert('Error');
            });
          }
          chars = [];
          pressed = false;
        },500);
      }
      pressed = true;
    });
});