class ItemsController < ApplicationController
  # before_action :set_item, only: [:show, :edit, :update, :destroy, :add_stock, :generate_code]
  # load_and_authorize_resource
  load_resource find_by: :sequential_id, except: :update
  load_resource only: :update
  authorize_resource

  respond_to :html, :json, :js

  # require 'barby'
  # require 'barby/barcode/ean_13'
  # require 'barby/outputter/rmagick_outputter'
  # require "rmagick"

  # GET /items
  # GET /items.json
  def index
    @items = @items.where(category: params[:category_id]) if params[:category_id].present? # have to verify this line
    @items = @items.where("name like ?", "%#{params[:query]}%") if params[:query].present? # need this for ajax/json

    @q     = @items.ransack(params[:q])
    @items = @q.result(distinct: true)
    
    @items = @items.page(params[:page])
    respond_with @items
  end


  # GET /items/1
  # GET /items/1.json
  def show
    # if @item.type == 'bakery'
      # outputter = Barby::RmagickOutputter.new(Barby::EAN13.new(@item.code[0..-2]))
      # outputter.xdim = 1
      # outputter.height = 45
      # File.open("public/barcode2.png", 'wb'){|f| f.write outputter.to_png }

      # source = Magick::Image.read("public/barcode2.png").first
      # f = Magick::Image.new(230, 480) { self.density = 300 }
      # (0..6).each_with_index do |i|
      #   f.composite!(source , 115, (106*i)-10, Magick::OverCompositeOp)
      # end

      # f.write("public/barcode.png")
    # end
    @histories = @item.stock_histories
    respond_with @item, location: items_path(@item.sequential_id)
  end

  # GET /items/new
  def new
    respond_with @item
  end

  # GET /items/1/edit
  def edit
    respond_with @item, location: items_path(@item.sequential_id)
  end

  def edit_multiple
    @items = current_user.store.items.where(id: params[:item_ids])
  end

  # POST /items
  # POST /items.json
  def create
    @item = current_user.store.items.new(item_params)

    if @item.save
      @item.documents.create(image: params[:image])
      flash[:notice] = 'Item was successfully created.'
    end

    # respond_with @item, location: items_path(sequential_id: @item.sequential_id)
    # respond_with @item, location: item_path(@item.sequential_id)
    redirect_to items_path
  end

  def add_stock
    @item.add_stock params[:stock_quantity].to_f, params[:unit_price], params[:total_price]

    # respond_with @item, location: items_path(sequential_id: @item.sequential_id)
    respond_with @item, location: item_path(@item.sequential_id)
  end

  def discontinue
    respond_with @item.discontinued!, location: item_path(@item.sequential_id)
  end

  def activate
    respond_with @item.available!, location: item_path(@item.sequential_id)
  end
  
  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    if @item.update(item_params)
      @item.documents.create(image: params[:image])
      @item.create_activity key: 'item.update'
      flash[:notice] = 'Item was successfully updated.'
    end

    # respond_with @item, location: items_path(sequential_id: @item.sequential_id)
    respond_with @item, location: item_path(@item.sequential_id)
  end

  def update_multiple
    update_action = { 'add_stock' => 'add_stock', 'set_price' => 'set_price' }[params[:batch_action]]

    if update_action == 'add_stock'
      @items = current_user.store.items.where(id: params[:stock_quantities].keys)
      @items.each do |item|
        item.send(update_action, params[:stock_quantities][item.id.to_s], params[:unit_prices][item.id.to_s], params[:total_prices][item.id.to_s])
      end
    elsif update_action == 'set_price'
      @items = current_user.store.items.where(id: params[:sell_prices].keys)
      @items.each do |item|
        item.update(sell_price: params[:sell_prices][item.id.to_s])
      end
    end

    flash[:notice] = "Updated Items!"
    redirect_to items_path
  end

  # DELETE /items/1
  # DELETE /items/1.json
  # def destroy
  #   @item.destroy
  #   flash[:notice] = 'Item was successfully deleted.'

  #   respond_with @item, location: items_path(@item.sequential_id)
  # end

  def find_by_code
    @items = @items.where(code: params[:code])

    respond_with @items
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # def set_item
    #   @item = Item.find(params[:id])
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.permit(:image)
      params.require(:item).permit(:name, :buy_price, :sell_price, :stock, :code, :description, :category_id, :stock_threshold)
    end
end
