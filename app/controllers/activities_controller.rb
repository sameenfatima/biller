class ActivitiesController < ApplicationController
  # load_and_authorize_resource class: PublicActivity::Activity
  respond_to :html, :json

  def index
    # @activities = PublicActivity::Activity.order('created_at DESC')
    # @activities = @activities.order('created_at DESC')
    # respond_with @activities#.include(:user, :item, :order)

    @activities = @activities.page(params[:page])
    respond_with @activities
  end

  private
  
end
