class OrdersController < ApplicationController
  load_and_authorize_resource find_by: :sequential_id
  # load_resource find_by: :sequential_id, except: [:update]
  # load_resource only: [:update]
  # authorize_resource

  respond_to :html, :json

  def index
    @q      = @orders.ransack(params[:q])
    @orders = @q.result(distinct: true)

    @orders = @orders.order("cashout_time desc").page(params[:page])
  end

  def new
  end

  def show
    redirect_to order_ready_order_path(@order.sequential_id) if @order.draft?
  end

  def order_ready
  end

  def cash_out
    @order.paid! params[:order][:payment]

    redirect_to order_path(@order.sequential_id)
  end
  
  def return
  end

  def mark_void
    @order.void!

    redirect_to order_path(@order.sequential_id)
  end

  def create
    if params[:item_ids].present?
      @order = current_user.orders.new(store_id: current_user.store_id)
      @order.draft! params[:item_ids]

      if @order.valid?
        if @order.items.length == 0
          @order.destroy
          flash[:error] = 'Order cannot be empty!'
          redirect_to new_order_path
        else
          flash[:notice] = 'Order created successfully.'
          redirect_to order_ready_order_path(@order.sequential_id)
        end
      else
        flash[:error] = 'Item quantity not available'
        redirect_to new_order_path
      end
    else
      flash[:error] = 'Order cannot be empty!'
      redirect_to new_order_path
    end
  end

  def order_return
    if params[:item_ids].present?
      @order = current_user.orders.new(store_id: current_user.store_id, cashout_time: DateTime.now)
      @order.return! params[:item_ids]

      if @order.valid?
        if @order.items.length == 0
          @order.destroy
          flash[:error] = 'Order cannot be empty!'
          redirect_to :back
        else
          flash[:notice] = 'Order returned successfully.'
          redirect_to order_path(@order.sequential_id)
        end
      else
        flash[:notice] = 'Some error occured.'
        redirect_to :back
      end
    else
      flash[:error] = 'Order cannot be empty!'
      redirect_to :back
    end
  end

  # def destroy
  #   flash[:notice] = 'Order was deleted successfully.' if @order.destroy
  #   respond_with(@order)
  # end

  private
    # def set_order
    #   @order = Order.find(params[:id])
    # end

    # def order_params
    #   params.require(:order).permit(:status, :cashout_time, :total, :payment, :item_ids)
    # end
end
