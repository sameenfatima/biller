class ReportsController < ApplicationController
  # skip_authorization_check
  load_and_authorize_resource :item, parent: false
  load_and_authorize_resource :order, parent: false

  def index

    @total_items  = @items.count
    # @retail_stock = @items.where(type: 'retail').sum(:stock)
    # @bakery_stock = @items.where(type: 'bakery').sum(:stock)
    # @retail_items_worth = @items.where(type: 'retail').sum('stock * sell_price')
    # @bakery_items_worth = @items.where(type: 'bakery').sum('stock * sell_price')

    # @orders_today = @orders.where('cashout_time > ?', DateTime.now.beginning_of_day)
    # @order_week   = @orders.where('cashout_time > ?', DateTime.now.beginning_of_week)
    # @order_month  = @orders.where('cashout_time > ?', DateTime.now.beginning_of_month)
  end

  def order_by_hour
    @orders_by_hour = @orders.where(state: 'paid').group_by_hour_of_day(:cashout_time, format: "%l %P").count
    @data = @orders_by_hour.collect { |key, value| {"time" => key, "sales"=> value} }
	  respond_to do |format|
	  format.json {
	    render :json => @data.to_json
	  }
    end
  end

  def most_used_items
    @items.limit(5).order(:sale_count).pluck(:name, :sale_count, :id)
  end

end
