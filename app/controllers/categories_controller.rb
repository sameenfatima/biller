class CategoriesController < ApplicationController
  respond_to :html, :json, :js

  def index
    @categories = Category.all
  end

  def show
  end
end
