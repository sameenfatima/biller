class ExpensesController < InheritedResources::Base
  # respond_to :html, :xml, :json
  # respond_to :js, :only => :create
  # respond_to :iphone, :except => [ :edit, :update ]
  # actions :index, :show, :new, :create
  # defaults :resource_class => User, :collection_name => 'users', :instance_name => 'user'
  load_and_authorize_resource

  def create
    @expense.user_id = current_user.id
    create! { expenses_path }
  end

  private
  def expense_params
    params.require(:expense).permit(:amount, :date, :is_paid, :description)
  end
end

