class StoreController < ApplicationController
  before_action :set_store#, only: [:show, :edit, :update, :destroy, :settings]
  respond_to :html, :js, :json
  # load_and_authorize_resource

  def settings
  end

  def employees
    @employees = @store.employees.includes(:attendences)
    @date = params[:date] || DateTime.now.beginning_of_day
    @attendences = Attendence. joins(:user).where('users.id', @employees.pluck(:id)).where('attendences.created_at > ? and attendences.created_at < ?', @date.to_datetime, @date.to_datetime.end_of_day)
    # @employees = @employees.joins(:attendences).where('attendences.created_at > ? and attendences.created_at < ?', params[:date].to_datetime, params[:date].to_datetime.end_of_day) if params[:date].present?
  end

  def new_employee
    user = @store.users.new(name: params[:name], email: params[:email], password: params[:password], password_confirmation: params[:password_confirmation], monthly_salary: params[:monthly_salary], joined: params[:joined], is_active: true)

    if user.save
      flash[:notice] = 'New employee successfully created'
    else
      flash[:notice] = user.errors
    end
    redirect_to :back
  end

  def edit_employee
    user = @store.users.find_by(sequential_id: params[:sequential_id])
    user.update(name: params[:name], email: params[:email], monthly_salary: params[:monthly_salary], joined: params[:joined], is_active: true)

    if params[:password].present? && params[:password_confirmation].present?
      user.update(password: params[:password], password_confirmation: params[:password_confirmation])
    end

    flash[:notice] = 'Employee successfully updated' if user.valid?

    redirect_to :back
  end

  def mark_present
    user_ids = @store.employees.where(sequential_id: params[:sequential_ids]).select(:id)
    @store.employees.mark_present user_ids

    respond_with success: 'success'
  end

  def mark_absent
    user_ids = @store.employees.where(sequential_id: params[:sequential_ids]).select(:id)
    @store.employees.mark_absent user_ids

    respond_with success: 'success'
  end


  # GET /stores
  # GET /stores.json
  # def index
  #   @store = Store.all
  #   @order = Order.new  
  # end

  # GET /stores/1
  # GET /stores/1.json
  def show
  end

  # GET /stores/new
  # def new
  #   @store = Store.new
  # end

  # GET /stores/1/edit
  def edit
  end

  # POST /stores
  # POST /stores.json
  # def create
  #   @store = Store.new(store_params)

  #   respond_to do |format|
  #     if @store.save
  #       format.html { redirect_to @store, notice: 'Store was successfully created.' }
  #       format.json { render :show, status: :created, location: @store }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @store.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /stores/1
  # PATCH/PUT /stores/1.json
  def update
    respond_to do |format|
      if @store.update(store_params)
        if params[:image].present?
          @store.logo.destroy if @store.logo.present?
          @store.create_logo(image: params[:image])
        end
        format.html { redirect_to store_settings_path, notice: 'Store was successfully updated.' }
        format.json { render :settings, status: :ok}
      else
        format.html { render :edit }
        format.json { render json: @store.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stores/1
  # DELETE /stores/1.json
  # def destroy
  #   @store.destroy
  #   respond_to do |format|
  #     format.html { redirect_to stores_url, notice: 'Store was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_store
      # @store = Store.find(params[:id])
      @store = current_user.store
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def store_params
      params.permit(:logo)
      params.require(:store).permit(:name,:address)
    end
end
