class ToDosController < InheritedResources::Base
  before_action :load_to_do, except: :index
  load_and_authorize_resource
  respond_to :html, :json, :js

  def mark_done
     @to_do.update(is_done: true) 
     redirect_to action: :index
  end

  private

  def to_do_params
    params.require(:to_do).permit(:title, :description, :store_id, :user_id, :due_date, :alert_type)
  end

  def load_to_do
    @to_do = current_user.store.to_dos.where(sequential_id: params[:id]).first
  end

end

