class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!       # devise authentication
  before_action :load_activities
  include PublicActivity::StoreController # News Feed / notifaction, to save current_user

  #GET /
  def dashboard
    @order = Order.new  
    @most_used_items = current_user.store.items.most_used_items
    render 'dashboard/index'
  end

  protected
  # load_and_authorize_resource :activity, parent: false, class: PublicActivity::Activity # load public activity
  def load_activities
    # @activities = PublicActivity::Activity.where(store_id: current_user.store_id).order('created_at DESC') if user_signed_in?
    @activities = PublicActivity::Activity.where(store_id: current_user.store_id).order('created_at DESC').includes(:owner, trackable: [:activities]) if user_signed_in?
  end

  # cancancan redirect
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

  # active admin authentication function
  def authenticate_active_admin_user!
   authenticate_user!
   unless current_user.is_admin?
        flash[:alert] = "You are not authorized to access this resource!"
        redirect_to root_path
     end
  end
end
