json.extract! @expense, :id, :amount, :date, :is_paid, :description, :store_id, :user_id, :created_at, :updated_at
