json.array!(@expenses) do |expense|
  json.extract! expense, :id, :amount, :date, :is_paid, :description, :store_id, :user_id
  json.url expense_url(expense, format: :json)
end
