json.array!(@to_dos) do |to_do|
  json.extract! to_do, :id, :title, :description, :store_id, :user_id, :due_date, :alert_type
  json.url to_do_url(to_do, format: :json)
end
