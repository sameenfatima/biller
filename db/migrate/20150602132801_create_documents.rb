class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.integer :owner_id
      t.string :owner_type

      t.timestamps null: false
    end
  end
end
