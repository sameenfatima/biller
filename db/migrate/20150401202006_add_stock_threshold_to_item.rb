class AddStockThresholdToItem < ActiveRecord::Migration
  def change
    add_column :items, :stock_threshold, :integer
  end
end
