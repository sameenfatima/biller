class AddSequentialIdToItems < ActiveRecord::Migration
  def change
    add_column :items, :sequential_id, :integer
  end
end
