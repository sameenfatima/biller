class RenameApprovedToIsActive < ActiveRecord::Migration
  def change
    rename_column :users, :approved, :is_active
  end
end
