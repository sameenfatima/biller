class AddAdminIdToStore < ActiveRecord::Migration
  def change
    add_column :stores, :admin_id, :integer
  end
end
