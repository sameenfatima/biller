class CreateAttendences < ActiveRecord::Migration
  def change
    create_table :attendences do |t|
      t.integer :user_id
      t.datetime :date
      t.boolean :present

      t.timestamps null: false
    end
  end
end
