class AddBalanceToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :balance, :integer
  end
end
