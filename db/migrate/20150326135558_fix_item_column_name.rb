class FixItemColumnName < ActiveRecord::Migration
  def change
    change_table :items do |t|
      t.rename :title, :name
      t.rename :item_type, :type
    end
  end
end
