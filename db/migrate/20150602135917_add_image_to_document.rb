class AddImageToDocument < ActiveRecord::Migration
  def self.up
    add_attachment :documents, :image
  end

  def self.down
    remove_attachment :documents, :image
  end
end
