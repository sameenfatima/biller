class ChangeDefaultForStockThreshold < ActiveRecord::Migration
  def change
    change_column :items, :stock_threshold, :integer, default: 2
  end
end
