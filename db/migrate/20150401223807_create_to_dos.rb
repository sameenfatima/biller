class CreateToDos < ActiveRecord::Migration
  def change
    create_table :to_dos do |t|
      t.string :title
      t.text :description
      t.integer :store_id
      t.integer :user_id
      t.datetime :due_date
      t.string :alert_type

      t.timestamps null: false
    end
  end
end
