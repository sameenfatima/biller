class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :status
      t.datetime :cashout_time
      t.integer :total
      t.integer :payment

      t.timestamps
    end
  end
end
