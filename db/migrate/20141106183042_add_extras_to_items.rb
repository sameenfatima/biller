class AddExtrasToItems < ActiveRecord::Migration
  def change
    add_column :items, :price, :integer, default: 0, null: false
    add_column :items, :stock, :integer, default: 0, null: false
    add_column :items, :code, :string, default: 0
  end
end
