class AddStoreIdToStockHistories < ActiveRecord::Migration
  def change
    add_column :stock_histories, :store_id, :integer
  end
end
