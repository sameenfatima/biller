class AddSequentialIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :sequential_id, :integer
  end
end
