class ChangeUserIdToStoreId < ActiveRecord::Migration
  def change
    rename_column :orders, :user_id, :store_id
  end
end
