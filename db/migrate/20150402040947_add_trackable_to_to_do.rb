class AddTrackableToToDo < ActiveRecord::Migration
  def change
    add_column :to_dos, :trackable_id, :integer
    add_column :to_dos, :trackable_type, :string
  end
end
