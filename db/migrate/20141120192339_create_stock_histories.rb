class CreateStockHistories < ActiveRecord::Migration
  def change
    create_table :stock_histories do |t|
      t.integer :item_id
      t.integer :stock_quantity
      t.integer :unit_price
      t.integer :total_price
      
      t.timestamps
    end
  end
end
