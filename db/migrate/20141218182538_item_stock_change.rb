class ItemStockChange < ActiveRecord::Migration
  def change
    change_column :items, :stock, :decimal, precision: 10, scale: 2
  end
end
