class RemoveRoleFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :role, :string
    add_column    :users, :is_admin, :boolean, :deafult => false
  end
end
