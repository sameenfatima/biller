class UpdateItems < ActiveRecord::Migration
  def change
    change_column :items, :buy_price, :decimal, precision: 10, scale: 2
    change_column :items, :sell_price, :decimal, precision: 10, scale: 2

    change_column :stock_histories, :unit_price, :decimal, precision: 10, scale: 2
    change_column :stock_histories, :total_price, :decimal, precision: 10, scale: 2

    change_column :orders, :total, :decimal, precision: 10, scale: 2
    change_column :orders, :payment, :decimal, precision: 10, scale: 2
  end
end
