class AddIsDoneToToDo < ActiveRecord::Migration
  def change
    add_column :to_dos, :is_done, :boolean, default: false
  end
end
