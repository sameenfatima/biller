class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.integer :amount
      t.datetime :date
      t.boolean :is_paid
      t.text :description
      t.integer :store_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
