class Changecode < ActiveRecord::Migration
  def change
    change_column :items, :code, :string, default: nil
  end
end
