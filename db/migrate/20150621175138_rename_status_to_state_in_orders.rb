class RenameStatusToStateInOrders < ActiveRecord::Migration
  def change
    rename_column :orders, :status, :state
  end
end
