class AddSaleCountToItems < ActiveRecord::Migration
  def change
    add_column :items, :sale_count, :integer, :default => 0
  end
end
