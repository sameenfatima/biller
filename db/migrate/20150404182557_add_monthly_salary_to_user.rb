class AddMonthlySalaryToUser < ActiveRecord::Migration
  def change
    add_column :users, :monthly_salary, :integer
    add_column :users, :joined, :datetime
  end
end
