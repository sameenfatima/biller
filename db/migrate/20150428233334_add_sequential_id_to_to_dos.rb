class AddSequentialIdToToDos < ActiveRecord::Migration
  def change
    add_column :to_dos, :sequential_id, :integer
  end
end
