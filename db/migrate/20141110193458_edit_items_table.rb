class EditItemsTable < ActiveRecord::Migration
  def change
    remove_column :items, :price, :integer
    add_column :items, :buy_price, :integer
    add_column :items, :sell_price, :integer
  end
end
