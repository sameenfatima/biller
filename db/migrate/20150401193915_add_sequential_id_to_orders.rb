class AddSequentialIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :sequential_id, :integer
  end
end
