Rails.application.routes.draw do
  
  get 'reports/order_by_hour', defaults: { format: :json }
  
  resources :expenses

  resources :to_dos do
    member do
      get 'mark_done'
    end
  end

  resources :categories, only: [:index, :show] do
    resources :items
  end

  resources :activities, only: [:index]

  resources :store, only: [:edit, :update]
  get 'store/settings'          =>'store#settings'
  get 'store/employees'         =>'store#employees'
  post 'store/new_employee'     =>'store#new_employee'
  patch 'store/edit_employee'   =>'store#edit_employee'
  get 'store/mark_present'      =>'store#mark_present'
  get 'store/mark_absent'       =>'store#mark_absent'

  # resources :store, only: [] do
  # resources :stores do
  #   member do
  #     get 'settings'
  #     get 'employees'
  #     post 'new_employee'
  #     patch 'edit_employee'
  #   end
  # end

  resources :reports do
    # get 'daily_revenue'
  end
  
  resources :orders do
    resources :items
    member do
      get   'order_ready'
      get   'return'
      post  'order_return'
      patch 'cash_out'
      patch 'mark_void'
    end
  end

  resources :items do
    member do
      patch 'add_stock'
      patch 'discontinue'
      patch 'activate'
    end
    collection do
      get 'find_by_code'
      get 'edit_multiple'
      post  'update_multiple'
      patch 'update_multiple'
    end
  end

  devise_for :users, controllers: { registrations: 'users/registrations' }

  # resources :admin do
  #   collection do
  #     get 'approve'
  #     get 'make_admin'
  #     get 'destroy_user'
  #     get 'get_unapproved'
  #   end
  # end 

  #resources :sessions

  # get "items/search_item/(:name)", to: "items#search_item", as: :search_item

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'application#dashboard'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
